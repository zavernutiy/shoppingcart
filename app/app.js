import React from "react";
import {render} from "react-dom";

import App from "./components/App/App";

render(<App/>, document.getElementById("app"));

// Webpack Hot Module Replacement API
if (module.hot) {
    module.hot.accept('./components/App/App', () => { render(App) })
}