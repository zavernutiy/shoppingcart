import React, {Component} from "react";
import AppBar from "material-ui/AppBar";
import IconButton from 'material-ui/IconButton';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';

class AppBarComponent extends Component {
    constructor(props) {
        super(props);
    }

    getElementRight = () => {
        let {items} = this.props;

        return (
            <IconMenu
                iconButtonElement={
                    <IconButton><MoreVertIcon /></IconButton>
                }
                targetOrigin={{horizontal: 'right', vertical: 'top'}}
                anchorOrigin={{horizontal: 'right', vertical: 'top'}}>
                {items ? items.map(item => {
                    return <MenuItem key={item.id} primaryText={item.text} onClick={item.click.bind(null, item.id)}/>
                }) : null}
            </IconMenu>
        );
    };

    render() {
        return (
            <AppBar
                title="Shopping Cart"
                showMenuIconButton={false}
                iconElementRight={this.getElementRight()}
            />
        );
    }
}

export default AppBarComponent;