import React, {Component} from 'react';
import PropTypes from "prop-types";
import {GridList} from 'material-ui/GridList';
import GridItem from "./GridItem";
import withWidth, {LARGE} from 'material-ui/utils/withWidth';

import styles from "./Grid.scss";

class GridView extends Component {
    constructor(props) {
        super(props);
    }

    getGridList = () => {
        let {width} = this.props;
        let columns = 1;
        let gridWidth = 400;

        if (width === LARGE) {
            columns = 2;
            gridWidth = 800;
        }

        let gridDivStyle = {
            width: gridWidth,
            margin: "auto",
            border: "1px solid grey"
        };

        return (
            <div style={gridDivStyle}>
                <GridList
                    cols={columns}
                    padding={1}
                    cellHeight={400}>
                    {this.getGridItems()}
                </GridList>
            </div>
        );
    };

    getGridItems = () => {
        let {items, manageCart} = this.props;

        if (items) {
            return items.map(item => {
                return (
                    <GridItem
                        key={item.id}
                        id={item.id}
                        title={item.title}
                        description={item.description}
                        price={item.price}
                        image={item.image}
                        isInCart={item.isInCart}
                        manageCart={manageCart}
                    />
                )
            });
        }
    };

    render() {
        return (
            <div className={styles.container}>
                {this.getGridList()}
            </div>
        );
    }
}

GridView.propTypes = {
    items: PropTypes.array.isRequired
};


// Using material-ui withWidth HoC to make GridView responsive
export default withWidth()(GridView);