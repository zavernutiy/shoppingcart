"use strict";

import React from "react";
import {GridTile} from 'material-ui/GridList';
import ShoppingCartToggle from "../ShoppingCartToggle/ShoppingCartToggle";

const GridItem = (props) => {
    let {id, title, description, price, image, isInCart, manageCart} = props;

    let actionIcon = <ShoppingCartToggle id={id} isInCart={isInCart} manageCart={manageCart} color="white"/>;

    return (
        <GridTile
            title={`${title}, ${price}$`}
            subtitle={description}
            actionIcon={actionIcon}
            actionPosition="left"
            titlePosition="top"
            titleBackground="linear-gradient(to bottom, rgba(0,0,0,0.7) 0%,rgba(0,0,0,0.3) 70%,rgba(0,0,0,0) 100%)"
            cols={1}
            rows={1}>
            <img src={image}/>
        </GridTile>
    );
};

export default GridItem;