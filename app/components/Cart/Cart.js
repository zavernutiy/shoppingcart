import React, {Component} from 'react';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import ListView from "../List/ListView";

import dataAPI from "../../../lib/dataAPI";
import style from "./Cart.scss";

class Cart extends Component {
    constructor(props) {
        super(props);
    }

    getActionButtons = () => {
        let {close, purchase} = this.props;

        return [
            <FlatButton
                label="Continue shopping"
                primary={true}
                onClick={close}
            />,
            <FlatButton
                label="Purchase"
                primary={true}
                keyboardFocused={true}
                onClick={purchase}
            />
        ];
    };

    getListView = () => {
        let {cart, manageCart} = this.props;
        let items = dataAPI.getItemsByIdArray(cart);

        let total = 0;
        items.forEach(item => {
            total += item.price;
        });

        return items && items.length > 0 ?
            <div>
                <ListView items={items} manageCart={manageCart}/>
                <span className={style.right}>Total: {total}$</span>
            </div>
            : "You did not add any items to the cart";
    };

    render() {
        let {open, close} = this.props;
        let bodyStyle = {
            minHeight: 480
        };

        return (
            <div>
                <Dialog
                    title="Your shopping cart"
                    actions={this.getActionButtons()}
                    modal
                    open={open}
                    onRequestClose={close}
                    autoScrollBodyContent={true}
                    contentClassName={style.content}
                    bodyStyle={bodyStyle}
                >
                    {this.getListView()}
                </Dialog>
            </div>
        );
    }
}

export default Cart;