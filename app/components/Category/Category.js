import React, {Component} from "react";
import PropTypes from "prop-types";
import GridView from "../Grid/GridView";
import ListView from "../List/ListView";

import dataAPI from "../../../lib/dataAPI";
import styles from "./Category.scss";

class Category extends Component {
    constructor(props) {
        super(props);
    }

    getData = () => {
        let {displayType, categoryId, cart, manageCart} = this.props;
        let items = dataAPI.getItemsByCategoryId(categoryId);

        for (let item of items) {
            item.isInCart = cart.indexOf(item.id) !== -1;
        }

        return displayType === 0 ? <GridView items={items} manageCart={manageCart}/> :
            <ListView items={items} manageCart={manageCart}/>;
    };

    render() {
        return (
            <div className={styles.container}>
                {this.getData()}
            </div>
        );
    }
}

Category.propTypes = {
    displayType: PropTypes.number.isRequired,
    categoryId: PropTypes.string.isRequired
};

export default Category;