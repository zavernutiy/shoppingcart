"use strict";

import React from "react";
import ShoppingCartToggle from "../ShoppingCartToggle/ShoppingCartToggle";

import styles from "./List.scss";

const ListItem = (props) => {

    let {id, title, description, image, price, isInCart, manageCart} = props;

    return (
        <div className={styles.listItem}>
            <img className={styles.image} src={image}/>
            <div className={styles.content}>
                <p>{title}, {price}$</p>
                <span>{description}</span>
            </div>
            <div className={styles.right}>
                <ShoppingCartToggle id={id} isInCart={isInCart} manageCart={manageCart} color="grey"/>
            </div>
        </div>
    );
};

export default ListItem;