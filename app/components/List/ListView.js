import React, {Component} from "react";
import {List} from 'material-ui/List';
import ListItem from "./ListItem";
import withWidth, {LARGE} from 'material-ui/utils/withWidth';

class ListView extends Component {
    constructor(props) {
        super(props);
    }

    getList = () => {
        let {width} = this.props;
        let listWidth = 400;

        if (width === LARGE) {
            listWidth = 800;
        }

        let listDivStyle = {
            width: listWidth,
            margin: "auto",
            padding: 0,
            border: "1px solid grey"
        };

        let noPadding = {padding: 0};

        return (
            <div style={listDivStyle}>
                <List style={noPadding}>
                    {this.getListItems()}
                </List>
            </div>
        )
    };

    getListItems = () => {
        let {items, manageCart} = this.props;

        if (items) {
            return items.map(item => {
                return (
                    <ListItem
                        key={item.id}
                        id={item.id}
                        title={item.title}
                        description={item.description}
                        price={item.price}
                        image={item.image}
                        isInCart={item.isInCart}
                        manageCart={manageCart}
                    />
                )
            });
        }
    };

    render() {
        return (
            <div>
                {this.getList()}
            </div>
        );
    }
}

export default withWidth()(ListView);