import React, {Component} from "react";
import PropTypes from "prop-types";
import {Tabs, Tab} from 'material-ui/Tabs';
import SwipeableViews from 'react-swipeable-views';
import Category from "../Category/Category";

import dataAPI from "../../../lib/dataAPI";
import colors from "../../res/stylesheets/color.scss";

const inlineStyles = {
    headline: {
        fontSize: 24,
        paddingTop: 16,
        marginBottom: 12,
        fontWeight: 400,
    },
    slide: {
        padding: 10,
    },
    tabItem: {
        backgroundColor: colors.veryLightGrey
    },
    tabButton: {
        color: colors.extraDarkGrey
    },
    inkBar: {
        backgroundColor: colors.turquoise
    }
};

class TabsComponent extends Component {
    constructor(props) {
        super(props);

        this.state = {
            slideIndex: 0,
            slidesYOffset: [0, 0, 0]
        }
    }

    handleTabChange = (value) => {
        let {slideIndex, slidesYOffset} = this.state;

        // Save Y scroll position of current tab
        slidesYOffset[slideIndex] = window.pageYOffset;

        // Scroll next tab to its saved Y scroll position
        window.scrollTo(0, slidesYOffset[value]);

        this.setState({
            slideIndex: value,
            slidesYOffset
        });
    };

    getTabs = () => {
        return dataAPI.getCategories().map((category, i) => {
            return <Tab key={category.id} buttonStyle={inlineStyles.tabButton} label={category.name} value={i}/>
        });
    };

    getCategories = () => {
        let categories = dataAPI.getCategories();
        let {displayType, cart, manageCart} = this.props;

        return categories.map(category => {
            return (
                <Category key={category.id}
                          displayType={displayType}
                          categoryId={category.id}
                          cart={cart}
                          manageCart={manageCart}
                />
            )
        });
    };

    render() {
        let {slideIndex} = this.state;

        return (
            <div>
                <Tabs
                    onChange={this.handleTabChange}
                    value={slideIndex}
                    tabItemContainerStyle={inlineStyles.tabItem}
                    inkBarStyle={inlineStyles.inkBar}>
                    {this.getTabs()}
                </Tabs>
                <SwipeableViews
                    index={slideIndex}
                    enableMouseEvents
                    onChangeIndex={this.handleTabChange}>
                    {this.getCategories()}
                </SwipeableViews>
            </div>
        );
    }
}

TabsComponent.propTypes = {
    displayType: PropTypes.number.isRequired
};

export default TabsComponent;