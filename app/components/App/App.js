import React, {Component} from "react";
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import {AppContainer} from 'react-hot-loader'
import AppBar from "../AppBar/AppBar";
import Tabs from "../Tabs/Tabs";
import Cart from "../Cart/Cart";

class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            appBarItems: [
                {
                    id: 0,
                    text: "Gallery",
                    click: this.setDisplay
                },
                {
                    id: 1,
                    text: "List",
                    click: this.setDisplay
                },
                {
                    id: 2,
                    text: "Cart",
                    click: this.toggleCart
                },
            ],
            // displayType can be either 0 or 1. Where 0 - Gallery view, and 1 - List view
            displayType: 0,
            showCart: false,
            cart: []
        }
    }

    setDisplay = (displayType) => {
        this.setState({displayType});
    };

    /**
     * Change flag showCart to opposite. (If it was true, set to false and vice verse)
     */
    toggleCart = () => {
        this.setState({
            showCart: !this.state.showCart
        })
    };

    purchase = () => {
        this.setState({
            cart: [],
            showCart: false
        });
    };

    /**
     * Method that adds id in the cart if it was not there
     * And removes the id if it was already in the cart
     * @param id
     */
    manageCart = (id) => {
        let {cart} = this.state;

        if (cart.indexOf(id) === -1) {
            // Add to cart
            cart.push(id);
        } else {
            // Remove from cart
            cart = cart.filter(cartId => cartId !== id);
        }

        this.setState({cart});
    };

    render() {
        let {appBarItems, displayType, showCart, cart} = this.state;

        return (
            <AppContainer>
                <MuiThemeProvider>
                    <div>
                        <AppBar items={appBarItems}/>
                        <Tabs
                            displayType={displayType}
                            cart={cart}
                            manageCart={this.manageCart}/>
                        <Cart
                            open={showCart}
                            cart={cart}
                            manageCart={this.manageCart}
                            close={this.toggleCart}
                            purchase={this.purchase}/>
                    </div>
                </MuiThemeProvider>
            </AppContainer>
        )
    }
}

export default App;