"use strict";

import React from "react";
import IconButton from 'material-ui/IconButton';
import AddShoppingCart from 'material-ui/svg-icons/action/add-shopping-cart';
import RemoveShoppingCart from 'material-ui/svg-icons/action/remove-shopping-cart';

const ShoppingCartToggle = (props) => {
    let {id, isInCart, color, manageCart} = props;

    return <IconButton onClick={manageCart.bind(null, id)}>{isInCart ? <RemoveShoppingCart color={color}/> :
        <AddShoppingCart color={color}/>}</IconButton>;
};

export default ShoppingCartToggle;