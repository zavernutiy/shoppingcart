export default [
    {
        id: "id1",
        categoryId: "cID1",
        title: "OnePlus 5",
        description: "Android 7.1.1 (Nougat) + OxygenOS",
        price: 436,
        image: "https://www.mobilitaria.com/wp-content/uploads/2017/10/OnePlus-5-400x400.jpg"
    },
    {
        id: "id2",
        categoryId: "cID1",
        title: "LG Nexus 5X",
        description: "Android 6.0 (Marshmallow)",
        price: 265,
        image: "http://www.ctinyc.com/image/cache/catalog/MOBNESUS5BLACK132GB-gadget360[1]-400x400.jpg"
    },
    {
        id: "id3",
        categoryId: "cID1",
        title: "Apple iPhone 7",
        description: "iOS 10",
        price: 692,
        image: "http://www.buyondubai.com/template/images/products/medium/147335058014733505511473337348_iphone-7plus-black.jpg"
    },
    {
        id: "id4",
        categoryId: "cID1",
        title: "Google Pixel",
        description: "Android 7 (Nougat)",
        price: 709,
        image: "http://www.crn.com/ckfinder/userfiles/images/crn/slideshows/2016/google-pixel/Slide1.jpg"
    },
    {
        id: "id5",
        categoryId: "cID1",
        title: "OnePlus 5",
        description: "Android 7.1.1 (Nougat) + OxygenOS",
        price: 436,
        image: "https://www.mobilitaria.com/wp-content/uploads/2017/10/OnePlus-5-400x400.jpg"
    },
    {
        id: "id6",
        categoryId: "cID1",
        title: "LG Nexus 5X",
        description: "Android 6.0 (Marshmallow)",
        price: 265,
        image: "http://www.ctinyc.com/image/cache/catalog/MOBNESUS5BLACK132GB-gadget360[1]-400x400.jpg"
    },
    {
        id: "id7",
        categoryId: "cID1",
        title: "Apple iPhone 7",
        description: "iOS 10",
        price: 692,
        image: "http://www.buyondubai.com/template/images/products/medium/147335058014733505511473337348_iphone-7plus-black.jpg"
    },
    {
        id: "id8",
        categoryId: "cID1",
        title: "Google Pixel",
        description: "Android 7 (Nougat)",
        price: 709,
        image: "http://www.crn.com/ckfinder/userfiles/images/crn/slideshows/2016/google-pixel/Slide1.jpg"
    },
    {
        id: "id9",
        categoryId: "cID1",
        title: "OnePlus 5",
        description: "Android 7.1.1 (Nougat) + OxygenOS",
        price: 436,
        image: "https://www.mobilitaria.com/wp-content/uploads/2017/10/OnePlus-5-400x400.jpg"
    },
    {
        id: "id10",
        categoryId: "cID1",
        title: "LG Nexus 5X",
        description: "Android 6.0 (Marshmallow)",
        price: 265,
        image: "http://www.ctinyc.com/image/cache/catalog/MOBNESUS5BLACK132GB-gadget360[1]-400x400.jpg"
    },
    {
        id: "id11",
        categoryId: "cID1",
        title: "Apple iPhone 7",
        description: "iOS 10",
        price: 692,
        image: "http://www.buyondubai.com/template/images/products/medium/147335058014733505511473337348_iphone-7plus-black.jpg"
    },
    {
        id: "id12",
        categoryId: "cID1",
        title: "Google Pixel",
        description: "Android 7 (Nougat)",
        price: 709,
        image: "http://www.crn.com/ckfinder/userfiles/images/crn/slideshows/2016/google-pixel/Slide1.jpg"
    },
    {
        id: "id13",
        categoryId: "cID2",
        title: "OnePlus 5",
        description: "Android 7.1.1 (Nougat) + OxygenOS",
        price: 436,
        image: "https://www.mobilitaria.com/wp-content/uploads/2017/10/OnePlus-5-400x400.jpg"
    },
    {
        id: "id14",
        categoryId: "cID2",
        title: "LG Nexus 5X",
        description: "Android 6.0 (Marshmallow)",
        price: 265,
        image: "http://www.ctinyc.com/image/cache/catalog/MOBNESUS5BLACK132GB-gadget360[1]-400x400.jpg"
    },
    {
        id: "id15",
        categoryId: "cID2",
        title: "Apple iPhone 7",
        description: "iOS 10",
        price: 692,
        image: "http://www.buyondubai.com/template/images/products/medium/147335058014733505511473337348_iphone-7plus-black.jpg"
    },
    {
        id: "id16",
        categoryId: "cID2",
        title: "Google Pixel",
        description: "Android 7 (Nougat)",
        price: 709,
        image: "http://www.crn.com/ckfinder/userfiles/images/crn/slideshows/2016/google-pixel/Slide1.jpg"
    },
    {
        id: "id17",
        categoryId: "cID2",
        title: "OnePlus 5",
        description: "Android 7.1.1 (Nougat) + OxygenOS",
        price: 436,
        image: "https://www.mobilitaria.com/wp-content/uploads/2017/10/OnePlus-5-400x400.jpg"
    },
    {
        id: "id18",
        categoryId: "cID2",
        title: "LG Nexus 5X",
        description: "Android 6.0 (Marshmallow)",
        price: 265,
        image: "http://www.ctinyc.com/image/cache/catalog/MOBNESUS5BLACK132GB-gadget360[1]-400x400.jpg"
    },
    {
        id: "id19",
        categoryId: "cID2",
        title: "Apple iPhone 7",
        description: "iOS 10",
        price: 692,
        image: "http://www.buyondubai.com/template/images/products/medium/147335058014733505511473337348_iphone-7plus-black.jpg"
    },
    {
        id: "id20",
        categoryId: "cID2",
        title: "Google Pixel",
        description: "Android 7 (Nougat)",
        price: 709,
        image: "http://www.crn.com/ckfinder/userfiles/images/crn/slideshows/2016/google-pixel/Slide1.jpg"
    },
    {
        id: "id21",
        categoryId: "cID2",
        title: "OnePlus 5",
        description: "Android 7.1.1 (Nougat) + OxygenOS",
        price: 436,
        image: "https://www.mobilitaria.com/wp-content/uploads/2017/10/OnePlus-5-400x400.jpg"
    },
    {
        id: "id22",
        categoryId: "cID2",
        title: "LG Nexus 5X",
        description: "Android 6.0 (Marshmallow)",
        price: 265,
        image: "http://www.ctinyc.com/image/cache/catalog/MOBNESUS5BLACK132GB-gadget360[1]-400x400.jpg"
    },
    {
        id: "id23",
        categoryId: "cID2",
        title: "Apple iPhone 7",
        description: "iOS 10",
        price: 692,
        image: "http://www.buyondubai.com/template/images/products/medium/147335058014733505511473337348_iphone-7plus-black.jpg"
    },
    {
        id: "id24",
        categoryId: "cID2",
        title: "Google Pixel",
        description: "Android 7 (Nougat)",
        price: 709,
        image: "http://www.crn.com/ckfinder/userfiles/images/crn/slideshows/2016/google-pixel/Slide1.jpg"
    },
    {
        id: "id25",
        categoryId: "cID3",
        title: "OnePlus 5",
        description: "Android 7.1.1 (Nougat) + OxygenOS",
        price: 436,
        image: "https://www.mobilitaria.com/wp-content/uploads/2017/10/OnePlus-5-400x400.jpg"
    },
    {
        id: "id26",
        categoryId: "cID3",
        title: "LG Nexus 5X",
        description: "Android 6.0 (Marshmallow)",
        price: 265,
        image: "http://www.ctinyc.com/image/cache/catalog/MOBNESUS5BLACK132GB-gadget360[1]-400x400.jpg"
    },
    {
        id: "id27",
        categoryId: "cID3",
        title: "Apple iPhone 7",
        description: "iOS 10",
        price: 692,
        image: "http://www.buyondubai.com/template/images/products/medium/147335058014733505511473337348_iphone-7plus-black.jpg"
    },
    {
        id: "id28",
        categoryId: "cID3",
        title: "Google Pixel",
        description: "Android 7 (Nougat)",
        price: 709,
        image: "http://www.crn.com/ckfinder/userfiles/images/crn/slideshows/2016/google-pixel/Slide1.jpg"
    },
    {
        id: "id29",
        categoryId: "cID3",
        title: "OnePlus 5",
        description: "Android 7.1.1 (Nougat) + OxygenOS",
        price: 436,
        image: "https://www.mobilitaria.com/wp-content/uploads/2017/10/OnePlus-5-400x400.jpg"
    },
    {
        id: "id30",
        categoryId: "cID3",
        title: "LG Nexus 5X",
        description: "Android 6.0 (Marshmallow)",
        price: 265,
        image: "http://www.ctinyc.com/image/cache/catalog/MOBNESUS5BLACK132GB-gadget360[1]-400x400.jpg"
    },
    {
        id: "id31",
        categoryId: "cID3",
        title: "Apple iPhone 7",
        description: "iOS 10",
        price: 692,
        image: "http://www.buyondubai.com/template/images/products/medium/147335058014733505511473337348_iphone-7plus-black.jpg"
    },
    {
        id: "id32",
        categoryId: "cID3",
        title: "Google Pixel",
        description: "Android 7 (Nougat)",
        price: 709,
        image: "http://www.crn.com/ckfinder/userfiles/images/crn/slideshows/2016/google-pixel/Slide1.jpg"
    },
    {
        id: "id33",
        categoryId: "cID3",
        title: "OnePlus 5",
        description: "Android 7.1.1 (Nougat) + OxygenOS",
        price: 436,
        image: "https://www.mobilitaria.com/wp-content/uploads/2017/10/OnePlus-5-400x400.jpg"
    },
    {
        id: "id34",
        categoryId: "cID3",
        title: "LG Nexus 5X",
        description: "Android 6.0 (Marshmallow)",
        price: 265,
        image: "http://www.ctinyc.com/image/cache/catalog/MOBNESUS5BLACK132GB-gadget360[1]-400x400.jpg"
    },
    {
        id: "id35",
        categoryId: "cID3",
        title: "Apple iPhone 7",
        description: "iOS 10",
        price: 692,
        image: "http://www.buyondubai.com/template/images/products/medium/147335058014733505511473337348_iphone-7plus-black.jpg"
    },
    {
        id: "id36",
        categoryId: "cID3",
        title: "Google Pixel",
        description: "Android 7 (Nougat)",
        price: 709,
        image: "http://www.crn.com/ckfinder/userfiles/images/crn/slideshows/2016/google-pixel/Slide1.jpg"
    }
];