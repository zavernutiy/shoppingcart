"use strict";

import categories from "../app/res/data/categories";
import items from "../app/res/data/items";

let getCategories = () => {
    return categories;
};

let getItems = () => {
    return items;
};

let getElementById = (array, id) => {
    for (let element in array) {
        if (element.id === id) {
            return element;
        }
    }
};

let getCategoryById = (id) => {
    return getElementById(categories, id);
};

let getItemById = (id) => {
    return getElementById(items, id);
};

let getItemsByCategoryId = (categoryId) => {
    return items.filter(item => item.categoryId === categoryId);
};

let getItemsByIdArray = (idArray) => {
    return items.filter(item => idArray.indexOf(item.id) !== -1);
};

export default {
    getCategories,
    getItems,
    getCategoryById,
    getItemById,
    getItemsByCategoryId,
    getItemsByIdArray
};